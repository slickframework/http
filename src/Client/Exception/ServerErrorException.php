<?php

/**
 * This file is part of slick/http
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Slick\Http\Client\Exception;

use Slick\Http\Exception;

/**
 * ServerErrorResponse
 *
 * @package Slick\Http\Client\Exception
 */
class ServerErrorException extends HttpResponseException implements Exception
{

}
