<?php

/**
 * This file is part of slick/http
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Slick\Http\Client\Exception;

use RuntimeException as PhpException;
use Slick\Http\Exception;

/**
 * RuntimeException
 *
 * @package Slick\Http\Client\Exception
 */
class RuntimeException extends PhpException implements Exception
{

}
